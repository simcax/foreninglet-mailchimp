# ForeningLet --> Mailchimp synchronizer
A project enabling users of [ForeningLet](https://foreninglet.dk) to sync members to Mailchimp thus enabling sending nice newsletters

# Status
Right now only the basic integrations are in place. 

+ Read member from ForeningLet via API
+ Check Mailchimp if member is in an audience
+ Add member to Mailchimp audience
+ Remove member from Mailchimp audience

To do:

+ Run through all members and check current status - add if not there
+ Add command line arguments for actions
+ Make a mode available for continously synchronizing users to Mailchimp

# Developer Setup
```python
python -m pip install -r requirements.txt
```

There are no current external dependencies.

## Tests
The project uses pytest and is being developed using a TDD approach. 
The code will have to live up to PEP8, and is being checked with:

+ flake8
+ pylint ( score at least 9)

Plans are to implement black checking as well.

All tests are currently residing in /foreninglet_mailchimp/tests

