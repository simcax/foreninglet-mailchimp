"""Enables mocking of a member as returned from the ForeningLet API"""
from random import randint
from datetime import date, timedelta
from ..fl_types import Member
from .test_utils import TestUtils

# pylint: disable=too-few-public-methods
class MockMember(Member):
    """
    Extends the Member dataclass and adds a method to mock a member instance
    """

    genders = ["Mand", "Kvinde"]
    delivery_methods = ["Manuelt", "Email/Dankort", "Email/Udenlandsk kort"]
    member_field_1 = 1  # Document what this field signifies.
    member_field_2 = "Ja"  # Yes, but to what?
    member_field_3 = "240"  # What does this number signify??
    activity_ids = '86407' # 3 month membership activity id

    # Disabling no-member for this_member.to_json implemented by dataclass_json
    # pylint: disable=no-member
    def mock_member(self, output_type="json"):
        """Mocks a member instance of the dataclass Member type"""
        tu_obj = TestUtils()
        this_member = Member(
            MemberId=randint(0, 1000),
            MemberNumber=randint(0, 1000),
            MemberCode=tu_obj.create_random_string(),
            FirstName=tu_obj.create_random_string(),
            LastName=tu_obj.create_random_string(),
            Address=tu_obj.create_random_string(),
            Zip=randint(1000, 9999),
            City=tu_obj.create_random_string(),
            CountryCode="DK",
            Email=tu_obj.create_random_email(),
            Birthday=str(date.today()),
            Gender=self.genders[randint(0, 1)],
            Phone=randint(10000000, 99999999),
            Mobile=randint(10000000, 99999999),
            EnrollmentDate=str(date.today()),
            DeliveryMethod=self.delivery_methods[randint(0, 2)],
            Created=str(date.today() - timedelta(days=randint(0, 7))),
            Updated=str(date.today() - timedelta(days=randint(0, 7))),
            GenuineMember=1,
            activity_ids={'12345'}
        )
        if output_type == "json":
            return_obj = this_member.to_json()
        else:
            return_obj = this_member
        return return_obj
