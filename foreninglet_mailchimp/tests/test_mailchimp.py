"""Tests for the mailchimp client"""

# pylint: disable=unused-import
import hashlib
import pytest  # noqa: F401
from foreninglet_mailchimp import mc_interface
from .test_utils import TestUtils
from ..mc_interface import LfMailChimpIf


def test_mc_interface():
    """A test to see there is a correctly configured api endpoint"""
    mcif = mc_interface.LfMailChimpIf()
    ping_response = mcif.ping_mc()
    assert ping_response["health_status"] == "Everything's Chimpy!"


def test_user_exists_false():
    """Testing if a user exists in mailchimp"""
    mcif = mc_interface.LfMailChimpIf()
    user_exists = mcif.user_exists("non-existent-user@somedomain.com")
    assert user_exists is False


def test_user_exists_true(mc_member):
    """Testing if a user exists in mailchimp"""
    mcif = mc_interface.LfMailChimpIf()
    user_added = mcif.add_user(mc_member)
    user_exists = False
    if user_added:
        user_exists = mcif.user_exists(mc_member.EmailAddress)
    assert user_exists is True
    # Try to remove the user, as to not leave it lingering
    mcif.remove_user(mc_member.EmailAddress)


@pytest.fixture
def _mc_add_user(mc_member):
    """Fixture which adds a user to a mailchimp audience"""
    mcif = mc_interface.LfMailChimpIf()
    user_added = mcif.add_user(mc_member)
    yield user_added
    # Try to remove the user, as to not leave it lingering
    mcif.remove_user(mc_member.EmailAddress)


def test_add_user(mc_member):
    """Testing adding a user to mailchimp audience"""
    mcif = mc_interface.LfMailChimpIf()
    user_added = mcif.add_user(mc_member)
    assert user_added is True
    # Try to remove the user, as to not leave it lingering
    mcif.remove_user(mc_member.EmailAddress)


def test_email_hashed():
    """Testing hashing an email address"""
    testu = TestUtils()
    email = testu.create_random_email()
    mcif = LfMailChimpIf()
    hashed_email = mcif.hash_email(email)
    assert hashed_email == hashlib.md5(email.encode("utf-8").lower()).hexdigest()


def test_remove_user(mc_member):
    """Testing to remove a user from mailchimp audience"""
    mclf = mc_interface.LfMailChimpIf()
    user_added = mclf.add_user(mc_member)
    assert user_added is True
    user_removed = mclf.remove_user(mc_member.EmailAddress)
    assert user_removed is True

def test_adding_a_user_after_deletion(mc_member):
    mclf = mc_interface.LfMailChimpIf()
    # First add user
    user_added = mclf.add_user(mc_member)
    # Then delete the user
    user_deleted = mclf.remove_user(mc_member.EmailAddress)
    # Then add the user again...
    user_added_2nd_time = mclf.add_user(mc_member)
    assert user_added_2nd_time == user_deleted == user_added == True
    # Then remove the user finally
    deleted_user = mclf.remove_user(mc_member.EmailAddress)

def test_get_mailchimp_users():
    """Tests getting all mailchimp subscribers from a list id"""
    mcif = LfMailChimpIf()
    subscribers = mcif.get_subscribers()
    assert len(subscribers) > 0

def test_add_tag_to_subscriber(mc_member):
    """A test for adding tags to a mailchimp subscriber"""
    mcif = mc_interface.LfMailChimpIf()
    # Add a random user
    mcif.add_user(mc_member)
    tags = {'name': '3 måneders medlemskab', 'status': 'active'}
    tagged = mcif.add_tags(mc_member.EmailAddress, tags)
    assert tagged.ok is True
    mcif.remove_user(mc_member.EmailAddress)


# TODO: Implement test for getting tags from a mc subscriber
def test_get_tag_for_subscriber(mc_member):
    """A test to label a """
    pass