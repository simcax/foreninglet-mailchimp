from operator import index
import re
import this
import pytest

import json

from tomlkit import datetime
from ..forening_let import ForeningLet
from ..mc_interface import LfMailChimpIf
from ..foreninglet_sync import FlMcSync
from ..mc_types import MailchimpMember


def test_loop_load_users_to_object(fl_patched):
    """Test loading member to the foreninglet sync class"""
    sync_obj = FlMcSync()
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    sync_obj.load_fl_members(memberlist)
    assert sync_obj.member_count() == 10


def test_mapping_member_from_fl_to_mc(fl_member):
    """A test mapping a FL Member obj to a MC Member obj"""
    sync_obj = FlMcSync()
    mc_member = sync_obj.map_fl_member(fl_member)
    assert isinstance(mc_member, MailchimpMember)


def test_syncing_users_to_mailchimp(fl_patched):
    """Test adding members not currently in mailchimp"""
    sync_obj = FlMcSync()
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    sync_obj.load_fl_members(memberlist)
    result = sync_obj.sync_members()
    assert result == len(memberlist)
    _remove_members_on_list(memberlist)


def test_syncing_users_to_mailchimp_1(fl_patched):
    """Test adding members not currently in mailchimp, but where 1 user exists"""
    sync_obj = FlMcSync()
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    sync_obj.load_fl_members(memberlist)
    result = sync_obj.sync_members()
    assert result == len(memberlist)
    _remove_members_on_list(memberlist)
    _clear_list()

def _remove_members_on_list(memberlist):
    mcif_obj = LfMailChimpIf()
    print("Cleaning up")
    for member in memberlist:
        mcif_obj.remove_user(member.get('Email'))

def _clear_list():
    mcif_obj = LfMailChimpIf()
    subscribers = mcif_obj.get_subscribers()
    for subscriber in subscribers:
        mcif_obj.remove_user(subscriber.EmailAddress)

def test_get_users_on_list():
    mcif_obj = LfMailChimpIf()
    subscribers = mcif_obj.get_subscribers()
    assert len(subscribers) >= 1

