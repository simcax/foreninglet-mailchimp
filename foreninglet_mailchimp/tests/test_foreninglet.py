"""Tests ForeningLet functionality"""
# For pytest module not directly being used
# pylint: disable=unused-import
from os import environ
import json
import pytest
from ..forening_let import ForeningLet
from ..fl_types import Member
from .mock_member import MockMember
from ..mc_types import MailchimpMember
import time
import os

def test_api_username_cred_in_env():
    """
    Test to make sure the API_USERNAME was set in the environment
    - easy way to debug failing build
    """
    api_username = environ.get("API_USERNAME")
    assert len(api_username) == 4


def test_api_password_cred_in_env():
    """Test the API Password was set - easy way to debug failing build"""
    api_password = environ.get("API_PASSWORD")
    assert len(api_password) == 10


def test_api_url_in_env():
    """Test the API Base URL was set - easy way to debug failing build"""
    api_base_url = environ.get("API_BASE_URL")
    # It should be at least 10 chars long...
    assert len(api_base_url) > 10


def test_api_version_in_env():
    """Test the API_VERSION was set - easy way to debug failing build"""
    api_version = environ.get("API_VERSION")
    # It should be at least 10 chars long...
    assert len(api_version) == 9


def test_api_members_in_env():
    """Test the API_MEMBERS_API env. var was set - easy way to debug failing build"""
    api_members = environ.get("API_MEMBERS_API")
    # It should be at least 10 chars long...
    assert len(api_members) == 7


def test_check_api_is_responding():
    """Testing the API responds"""
    fl_obj = ForeningLet()
    status_code = fl_obj.check_api_responds()
    assert status_code == 200


def test_api_get_members():
    """Test it is possible to retrieve a python list of members"""
    begin = time.time()
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    end = time.time()
    duration = end-begin
    print(f"Duration: {duration} ")
    assert isinstance(memberlist, list)


def test_create_member_obj():
    """Tests creating a member object"""
    member = Member()
    assert isinstance(member, Member)


# Disabling no-member because the dataclass-json provides the from_dict method, not being picked up
# by pylint.
# Disabling unused-argument as we are pathing/mocking the memberlist by fixture fl_patched
# pylint: disable=no-member, unused-argument
def test_map_fl_api_member_to_member_object(fl_patched):
    """A test of mapping a ForeningLet member from the API to a FL member object"""
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    for member in memberlist:
        fl_member = Member.from_dict(member)
        break
    assert isinstance(fl_member, Member)


# Disabling unused-argument as we are pathing/mocking the memberlist by fixture fl_patched
# pylint: disable=no-member, unused-argument
def test_api_get_members_mocked(fl_patched):
    """Test getting the memberlist"""
    fl_obj = ForeningLet()
    memberlist = json.loads(fl_obj.get_memberlist())
    assert isinstance(memberlist, list)


def test_map_member_from_fl_to_mc():
    """Test mapping a member from ForeningLet to Mailchimp"""
    mm_obj = MockMember()
    member = mm_obj.mock_member(output_type="obj")
    mc_obj = MailchimpMember()
    mc_obj.ingest(member)
    assert mc_obj.Firstname == member.FirstName

def test_get_activities():
    """Test getting the full list of activities from ForeningLet"""
    fl_obj = ForeningLet()
    activitylist = fl_obj.get_activities()
    assert isinstance(activitylist,list)

def test_get_memberships_1(activity_data):
    """Test filtering memberships from the activity list"""
    fl_obj = ForeningLet()
    memberships = fl_obj.get_memberships(activity_data)
    assert isinstance(memberships,list)

def test_get_memberships_2(activity_data):
    """Test filtering memberships from the activity list"""
    fl_obj = ForeningLet()
    memberships = fl_obj.get_memberships(activity_data)
    assert len(memberships) == 1

def test_map_member_activities_to_membership(activity_list_patched):
    """
        Tests getting the membership on a member
    """
    os.environ['MEMBERSHIP_KEYWORDS'] = "'medlemsskab','medlemskab'"
    mm_obj = MockMember()
    member = mm_obj.mock_member(output_type="obj")
    assert '3 måneders' in member.Membership 
