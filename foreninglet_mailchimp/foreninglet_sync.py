"""
    Module to control synchronization from ForeningLet to Mailchimp api
    Uses the API exposed by ForeningLet to retrieve current member data
    and the MailchimpMarketing Python Module to interact with Mailchimp   
"""
from .mc_interface import LfMailChimpIf
from .fl_types import Member
from .mc_types import MailchimpMember


class FlMcSync:
    """Class to synchronize users from ForeningLet to Mailchimp"""

    foreninglet_memberlist = []

    def load_fl_members(self, memberlist):
        """Method to load members to the class"""
        self.foreninglet_memberlist = memberlist
        if len(self.foreninglet_memberlist) > 0:
            return True
        else:
            # We don't want to load an empty list of Members
            return False

    def member_count(self):
        """Simply returns the number of members loaded to the class list foreninglet_memberlist"""
        return len(self.foreninglet_memberlist)

    def sync_members(self):
        """
        Synchronizes Mailchimp with the Foreninglet memberlist.
        Takes no input, but the class needs to be primed
        with the load_fl_members function beforehand.
        Then this method will loop through that memberlist,
        and add missing members to the MailChimp audience.
        """
        users_added = 0
        users_failed = 0
        mcif_obj = LfMailChimpIf()
        mc_member = MailchimpMember()
        if self.member_count() > 0:
            for member in self.foreninglet_memberlist:
                fl_member = Member.from_dict(member)
                mc_member.ingest(fl_member)
                if mcif_obj.add_user(mc_member):
                    users_added += 1
                    print(f"Added user #{users_added}") 
                else:
                    users_failed += 1
        return users_added

    def map_fl_member(self, member):
        """
        Takes a Foreninglet Member object and maps it to a Mailchimp Member object
        Returns the Mailchimp Member Object
        """
        mc_member = MailchimpMember()
        mc_member.ingest(member)
        return mc_member

