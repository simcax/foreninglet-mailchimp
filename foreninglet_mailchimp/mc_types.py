"""A dataclass for a Mailchimp Member"""
from dataclasses import dataclass, field    
from dataclasses_json import dataclass_json
from .forening_let import ForeningLet

# pylint: disable=invalid-name
@dataclass_json
@dataclass
class MailchimpMember:
    """Implements a class for a member as to be imported to MailChimp"""

    Firstname: str = ""  # Firstname of member
    LastName: str = ""  # Surname/Lastname of member
    MemberNumber: int = 0  # The Members MemberNumber in the local association
    MemberId: int = 0  # The global ID of the Member in ForeningLet
    EmailAddress: str = ""  # The members email address
    MailchimpId: str = ""
    Tags: list = field(default_factory=list)

    def ingest(self, member):
        """Takes a ForeningLet Member Object and maps the values to a MailchimpMember object"""
        self.Firstname = member.FirstName
        self.LastName = member.LastName
        self.EmailAddress = member.Email
        self.MemberId = member.MemberId
        self.MemberNumber = member.MemberNumber
        self.MailchimpId = ""
        self.Tags = member.activity_ids

    