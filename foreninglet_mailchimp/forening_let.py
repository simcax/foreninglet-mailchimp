"""Class for connecting to ForeningLet API and retrieve users"""
import os
import json
import requests
from functools import lru_cache

class ForeningLet:
    """
    Interface to the ForeningLet API
    Will take the following from environment variables:
    API_PASSWORD = The password for the ForeningLet API
    API_USERNAME = The username for the ForeningLet API
    API_BASE_URL = Base URL for the ForeningLet API
    API_MEMBERS_API = The endpoint for the ForeningLet Member API
    API_ACTIVITIES_API = The endpoint for the ForeningLet Activities API
    API_VERSION = The version of the ForeningLet Member API
    MEMBERSHIP_KEYWORDS = Words qualifying membership activities
    """

    api_username = ""
    api_password = ""
    api_base_url = ""
    api_members_path = ""
    api_activities_path = ""
    api_version = ""
    api_members_url = ""
    api_activities_url = ""
    membership_word_list = ""

    def __init__(self) -> None:
        self.api_password = os.getenv("API_PASSWORD","")
        self.api_username = os.getenv("API_USERNAME","")
        self.api_base_url = os.getenv("API_BASE_URL","")
        self.api_members_path = os.getenv("API_MEMBERS_API","")
        self.api_activities_path = os.getenv("API_ACTIVITIES_API","")
        self.api_version = os.getenv("API_VERSION","")
        membership_keywords = os.getenv("MEMBERSHIP_KEYWORDS","")
        self.membership_word_list = []
        membership_keywords = membership_keywords.replace('"',"")
        membership_keywords = membership_keywords.replace("'","")
        for word in membership_keywords.split(','):
            self.membership_word_list.append(word)
        self.api_members_url = (
            f"{self.api_base_url}{self.api_members_path}?{self.api_version}"
        )
        self.api_activities_url = (
            f"{self.api_base_url}{self.api_activities_path}?{self.api_version}"
        )

    def fl_api_get(self, url):
        """
        Retrieves data from an api endpoint
        authenticates with the class api_username and api_password
        """
        resp = requests.get(url, auth=(self.api_username, self.api_password))
        return resp

    def check_api_responds(self):
        """Helper method to check the api endpoint responds"""
        resp = self.fl_api_get(self.api_members_url)
        return resp.status_code

    def get_memberlist(self):
        """Retrieves members from the member API endpoint"""
        resp = self.fl_api_get(self.api_members_url)
        return resp.text

    @lru_cache
    def get_activities(self):
        """Retrieve the entire activity list from the activity list endpoint"""
        resp = self.fl_api_get(self.api_activities_url)
        return json.loads(resp.text)

    def get_memberships(self, activity_list:dict) -> list:
        """
            Filters the memberships out of a list with all the activities
            Class var 'membership_word_list' needs to be configured,
            with words to identify membership activities with
            Input: dictionary with all activities
            Output: List of dicts of memberships
        """
        memberships = []
        for activity in activity_list:
            for value in activity.values():
                if any( membership_word in value for membership_word in self.membership_word_list):
                    memberships.append(activity)
                    break
        return memberships