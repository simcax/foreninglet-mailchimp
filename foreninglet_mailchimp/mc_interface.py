"""Class to interface with MailChimp"""

from os import environ
import hashlib
from mailchimp3 import MailChimp
import mailchimp_marketing as MailchimpMarketing
from mailchimp_marketing.api_client import ApiClientError
from .mc_types import MailchimpMember

class LfMailChimpIf:
    """
    A class to interact with the mailchimp module.
    Takes the following environment variables:
        MAILCHIMP_USERNAME      : the account email address to user when logging in
                                  (where the API key was generated)
        MAILCHIMP_KEY           : The API key as generated in Mailchimp
        MAILCHIMP_AUDIENCE_ID   : The list id for the audience in question
        MAILCHIMP_SERVER        : The url prefix for the mailchimp api - i.e. us3
    """

    mailchimp_username = ""
    mailchimp_key = ""
    mailchimp_list_id = ""
    mailchimp_server = ""

    def __init__(self):
        """The class initializes from environment values. In case they are not set, will err"""
        self.mailchimp_username = environ.get("MAILCHIMP_USERNAME", "novalue")
        self.mailchimp_key = environ.get("MAILCHIMP_KEY", "novalue")
        self.mailchimp_list_id = environ.get("MAILCHIMP_AUDIENCE_ID", "novalue")
        self.mailchimp_server = environ.get("MAILCHIMP_SERVER", "novalue")
        # Let's fail early - if an environment var has not been set as we expect.
        for avar in (
            "mailchimp_username",
            "mailchimp_key",
            "mailchimp_list_id",
            "mailchimp_server",
        ):
            self.__is_env_value_set(avar)

    def __is_env_value_set(self, variable_name):
        if getattr(self, variable_name) == "novalue":
            raise ValueError(f"{variable_name} vas not set in environment.")

    def mailchimp_client(self):
        client = MailchimpMarketing.Client()
        client.set_config(
                {"api_key": self.mailchimp_key, "server": self.mailchimp_server}
            )
        return client

    def ping_mc(self):
        """Ping the api endpoint, a way to check configuration was OK"""
        client = MailChimp(mc_user=self.mailchimp_username, mc_api=self.mailchimp_key)
        result = client.ping.get()
        return result

    def user_exists(self, email_address):
        """Method to check whether a user is on the audience targeted"""
        user_exists = False
        try:
            email_hash = self.hash_email(email_address)
            client = MailchimpMarketing.Client()
            client.set_config(
                {"api_key": self.mailchimp_key, "server": self.mailchimp_server}
            )
            response = client.lists.get_list_member(self.mailchimp_list_id, email_hash)
            if response["id"] != "":
                user_exists = True
        except ApiClientError as error:
            if error.status_code == 404:
                user_exists = False
            else:
                print(f"Error: {error.text}")
        return user_exists

    def add_user(self, mc_member):
        """
        Add a user to the targeted audience.
        Takes an mc_member object as input and maps values from that

        Returns:
        True = User added in case user already exists, or if user is added
        False = otherwise, any reason the user is not added
        """
        user_added = False
        try:
            client = MailchimpMarketing.Client()
            client.set_config(
                {"api_key": self.mailchimp_key, "server": self.mailchimp_server}
            )

            member_info = {
                "email_address": mc_member.EmailAddress,
                "status": "subscribed",
                "merge_fields": {
                    "FNAME": mc_member.Firstname,
                    "LNAME": mc_member.LastName,
                    "MEMBERNUM": mc_member.MemberNumber,
                    "MEMBERID": mc_member.MemberId,
                },
            }

            kwargs = {"skip_merge_validation": False}

            response = client.lists.add_list_member(
                self.mailchimp_list_id, member_info, **kwargs
            )
            if response["id"] != "":
                user_added = True
        except ApiClientError as error:
            if error.status_code == 400:
                # User already exists, so we're good.
                user_added = True
            else:
                print(f"Error: {error.text}")
        return user_added

    def remove_user(self, email_address):
        """Removes a user from the targeted audience. Takes an email address as input"""
        user_deleted = None
        try:
            email_hash = self.hash_email(email_address)
            client = MailchimpMarketing.Client()
            client.set_config(
                {"api_key": self.mailchimp_key, "server": self.mailchimp_server}
            )
            response = client.lists.delete_list_member(
                list_id=self.mailchimp_list_id, subscriber_hash=email_hash
            )
            if response.ok is True:
                user_deleted = True
        except ApiClientError as error:
            if error.status_code == 404:
                user_deleted = False
            print(f"Error: {error.text}")
        return user_deleted

    def hash_email(self, email):
        """Helper function to hash the email address before handing it to the mailchimp API"""
        return hashlib.md5(email.encode("utf-8").lower()).hexdigest()

    def get_subscribers(self) -> list:
        """
            Retrieves all subscribers from an audience
            Input: none
            Output: List of mc member objects
        """
        subscriber_list = []
        client = MailchimpMarketing.Client()
        client.set_config(
                {"api_key": self.mailchimp_key, "server": self.mailchimp_server}
            )
        subscribers = client.lists.get_list_members_info(self.mailchimp_list_id)
        #client.lists.get_list_clients()
        for subscriber in subscribers.get('members'):
            this_subscriber = MailchimpMember(
                subscriber['merge_fields'].get('FNAME'),
                subscriber['merge_fields'].get('LNAME'),
                subscriber['merge_fields'].get('MEMBERNUM',0),
                subscriber['merge_fields'].get('MEMBERID',0),
                subscriber['email_address'],
                subscriber['id']
            )
            subscriber_list.append(this_subscriber)
            
        return subscriber_list

    def add_tags(self, email_address: str, tags: dict) -> bool:
        """Method to add tags to a mailchimp subscriber"""
        client = self.mailchimp_client()
        subscriber_hash = self.hash_email(email_address)
        tag_dict = { 'tags': [tags]}
        response = client.lists.update_list_member_tags(self.mailchimp_list_id,subscriber_hash,tag_dict)
        return response
